#!/usr/bin/env python3
# -*- coding:utf-8 -*-

"""
Usage

Interfaces (dry run, verbose):
python3 netbox_inventory.py [--preprod-ansible-hosts-file <preprod.yml>] [--prod-ansible-hosts-file <prod.yml>] \
[--netbox-token <netbox_token>] [--netbox-url <netbox_api_url>] [-v/--verbose]

"""

import sys
import os
import argparse

from pprint import pprint
from helpers import init_logger
from collections import defaultdict

import pynetbox


def parse_machine(machine, target_env):
    """

    :param machine:
    :param target_env:
    :return:
    """
    ansible = False
    env = False
    active = False

    for tag in machine["tags"]:
        #  Ensure machine is handled by Ansible and has the corresponding env tag
        if target_env in tag["slug"]:
            env = True
        elif "ansible" in tag["slug"]:
            ansible = True

    if machine["status"]["label"] == "Active":
        active = True

    if ansible and env and active:
        return True


def item_lookup(item):
    """
    Find wanted attributes of a given device/VM
    """

    name = item["name"]  # The name of the device/VM
    # IPv4 try-catch block, items without a primary IPv4 defined are ignored
    try:
        ipv4 = item["primary_ip4"]["address"]
    except TypeError:
        return False

    if not ipv4:
        return False

    grp_tags = [x["slug"] for x in item["tags"] if "grp_" in x["slug"]]

    return {
        "name": name,
        "ipv4": ipv4,
        "grp_tags": grp_tags,
    }


class NetboxAnsibleInventory:
    """
    Create YAML inventory files from Netbox data (for comparison)
    """

    def __init__(
        self,
        netbox_token,
        netbox_api,
        preprod_output_file,
        prod_output_file,
        verbose=False,
    ):
        self.verbose = verbose
        self.log = init_logger(verbose=self.verbose)
        self.netbox_token = netbox_token
        self.netbox_api = netbox_api

        try:
            self.nb = pynetbox.api(self.netbox_api, token=self.netbox_token)
        except Exception as exc:
            self.log.fatal(
                f"Could not access Netbox API, either the API token or the URL is empty/incorrect "
                f"(Netbox API URL supplied: {self.netbox_api})"
            )
            sys.exit(exc)
        self.script_path = os.path.abspath(__file__)

        # Preprod
        self.preprod_machines = self.get_env_inventory(target_env="env_preprod")
        self.env_inventory_to_yaml_file(
            machines=self.preprod_machines,
            target_env="env_preprod",
            output_file=preprod_output_file,
        )

        # Prod
        self.prod_machines = self.get_env_inventory(target_env="env_prod")
        self.env_inventory_to_yaml_file(
            machines=self.prod_machines,
            target_env="env_prod",
            output_file=prod_output_file,
        )

    def get_env_inventory(self, target_env):
        # Validate machines
        self.log.info(f"Target env: {target_env}")
        target_env_machines = []
        self.log.info(f"Finding active devices with tag {target_env}...")
        _all_devices = self.nb.dcim.devices.all()
        for device in _all_devices:
            if parse_machine(device, target_env):
                target_env_machines.append(device)

        self.log.info(f"Finding preprod VMs with tag {target_env}...")
        _all_vms = self.nb.virtualization.virtual_machines.all()
        for vm in _all_vms:
            if parse_machine(vm, target_env):
                target_env_machines.append(vm)

        return target_env_machines

    def env_inventory_to_yaml_file(self, machines, target_env, output_file):

        tags_primary_dict = {}
        yaml_dict = {}

        self.log.debug(f"{len(machines)} machines found for  {target_env}")

        for _machine in machines:
            _machine_data = item_lookup(item=dict(_machine))
            if _machine_data:
                self.log.debug(
                    f"Machine {_machine_data['name']} has the following tags: {_machine_data['grp_tags']}"
                )
                for tag in _machine_data["grp_tags"]:
                    if tag not in tags_primary_dict.keys():
                        tags_primary_dict[tag] = []
                    tags_primary_dict[tag].append(
                        [_machine_data["name"], _machine_data["ipv4"]]
                    )

        with open(output_file, "w", encoding="utf-8") as fhandle:
            for key, value in tags_primary_dict.items():
                # self.log.debug(f"{key}:")
                fhandle.write(f"{key}:\n")
                # self.log.debug("  hosts:")
                fhandle.write("  hosts:\n")
                for l in value:
                    # self.log.debug(f"    - {l[-1]}  # {l[0]}")
                    fhandle.write(f"    - {l[-1]}  # {l[0]}\n")
                # self.log.debug("")
                fhandle.write("\n")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--netbox-token",
        type=str,
        required=False,
        default=os.environ.get("NETBOX_TOKEN", ""),
        help="Netbox API token, defaults to the environment variable NETBOX_TOKEN",
    )
    parser.add_argument(
        "--netbox-url",
        type=str,
        required=False,
        default=os.environ.get("NETBOX_API", ""),
        help="Netbox instance URL, defaults to the environment variable NETBOX_API",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        required=False,
        action="store_true",
        default=False,
        help='Enable "debug" level logs',
    )
    parser.add_argument(
        "--preprod-ansible-hosts-file",
        required=False,
        default=os.path.join(os.getcwd(), "preprod_inventory_hosts_file.yml"),
        help="Output file that will contain the preproduction inventory",
    )
    parser.add_argument(
        "--prod-ansible-hosts-file",
        required=False,
        default=os.path.join(os.getcwd(), "prod_inventory_hosts_file.yml"),
        help="Output file that will contain the production inventory",
    )
    arguments = parser.parse_args()

    _netbox_token = arguments.netbox_token
    _netbox_api = arguments.netbox_url

    NI = NetboxAnsibleInventory(
        netbox_token=_netbox_token,
        netbox_api=_netbox_api,
        verbose=arguments.verbose,
        preprod_output_file=arguments.preprod_ansible_hosts_file,
        prod_output_file=arguments.prod_ansible_hosts_file,
    )
