# netbox-utils

A collection of scripts to interact with a Netbox instance.

Python >= 3.6 is required.

### Usage

`python3 netbox_utils.py --csv <input_csv> <operation> (-v/--verbose) (-d/--dry-run) (-h/--help) (--yes)`

The script will ask for yes/no input when an existing object is going to be modified. 
To answer yes to all these prompts, use the `--yes` option or pipe `yes` before using the script. 

### Supported operations

Import new interfaces:
`python3 netbox_utils.py --import-interfaces --csv <input_csv>`
- Will also optionally allow to update the devices, interfaces and addresses that match the content of the csv file. 

Update existing devices:
`python3 netbox_utils.py --updates-devices --csv <input_csv>`

### How it works

The netbox_utils.py module takes as argument a "method" that has the following structure:

`--<action>-<affected-objects>` i.e. `--import-interfaces` or `--update-devices`

The module first creates an object derived from the `NetboxUtils` parent class, that serves to establish 
the connection to the Netbox instance API as well as load the input csv data.

### NB

Some objects are linked to other types of objects that can also be affected by the script.