#!/usr/bin/env python3
# -*- coding:utf-8 -*-

"""
Usage

Interfaces (dry run, verbose):
python3 netbox_utils.py --import-interfaces --csv interfaces.csv --dry-run --verbose --yes

"""

import sys
import os
import argparse

from pprint import pprint

import pynetbox

from helpers import lint_data, init_logger, yn_prompt, parse_input_data


class NetboxUtils:
    """
    Parent class defining all basic attributes (netbox instance, csv data, method, verbosity, ...)
    """

    def __init__(
        self,
        netbox_token,
        netbox_api,
        csv_input,
        verbose=False,
        dry_run=False,
        yes=False,
    ):
        self.verbose = verbose
        self.log = init_logger(verbose=self.verbose)
        self.dry_run = dry_run
        self.yes = yes

        if os.path.exists(csv_input):
            self.csv_input = os.path.abspath(csv_input)
        else:
            self.log.fatal(
                f"Provided CSV file does not exist or cannot be accessed ({csv_input})"
            )
            sys.exit(1)

        self.netbox_token = netbox_token
        self.netbox_api = netbox_api

        try:
            self.nb = pynetbox.api(self.netbox_api, token=self.netbox_token)
        except Exception as exc:
            self.log.fatal(
                f"Could not access Netbox API, either the API token or the URL is empty/incorrect "
                f"(Netbox API URL supplied: {self.netbox_api})"
            )
            sys.exit(exc)
        self.data = parse_input_data(csv_file=self.csv_input)
        self.script_path = os.path.abspath(__file__)

    def create_device(self, **kwargs):
        """

        :return:
        """
        device = self.nb.dcim.devices.create(**kwargs)

        return device

    def create_interface(self, **kwargs):
        """

        :return:
        """
        interface = self.nb.dcim.interfaces.create(**kwargs)

        return interface

    def create_ip_address(self, **kwargs):
        """

        :return:
        """
        ip_address = self.nb.ipam.ip_addresses.create(**kwargs)

        return ip_address

    def update_dict(self, obj_dict: dict, data_dict: dict) -> dict:
        """

        :param obj_dict:
        :param data_dict:
        :return:
        """
        ret_dict = dict()
        ret_dict.update(obj_dict)
        ret_dict.update(data_dict)

        if self.verbose:
            self.log.debug(f"Updated data: ")
            pprint(ret_dict)

        return ret_dict


class UpdateDevices(NetboxUtils):
    """ """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.required_fields = {"name", "site"}  # Add required fields (header)
        lint_data(
            logger=self.log,
            csv_file=self.csv_input,
            csv_data=self.data,
            required_fields=self.required_fields,
        )
        self.update_devices()

    @classmethod
    def __create_from__(cls, par: NetboxUtils):
        return cls(
            netbox_api=par.netbox_api,
            netbox_token=par.netbox_token,
            csv_input=par.csv_input,
            verbose=par.verbose,
            dry_run=par.dry_run,
        )

    def update_devices(self):
        """

        :return:
        """
        # Get devices info
        all_devices = self.nb.dcim.devices.all()  # Physical devices
        devices_names = [device.name for device in all_devices]

        updated_data_list = []

        for item in self.data:
            # Check if the current specified device in the CSV exists in Netbox
            if item["name"] not in devices_names:
                self.log.fatal(f"No device matching name {item['name']} found.")
                sys.exit(1)  # If device doesn't exist, exit
            else:
                filtered_devices = self.nb.dcim.devices.filter(name=item["name"])

                # The API forces looping over the returned device (no slicing possible)
                for device in filtered_devices:
                    current_data = dict(device)
                    updated_data = current_data

                    if current_data["site"]["display"] == item["site"]:
                        custom_fields = {}

                        for field, value in item.items():
                            if field.startswith("cf"):
                                custom_fields[str(field).replace("cf_", "")] = str(
                                    value
                                ).strip()

                        for cf_key, cf_value in custom_fields.items():
                            updated_data["custom_fields"][cf_key] = cf_value

                        for key in list(updated_data.keys()):
                            if key not in ("id", "custom_fields", "name"):
                                del updated_data[key]
                        updated_data_list.append(updated_data)

                        break
                    break

        self.log.info("Updating...")
        for device_dict in updated_data_list:
            self.log.debug(
                f"Updating device {device_dict['name']}\n  - ID:  {device_dict['id']}\n  - "
                f"data changed: {device_dict['custom_fields']}"
            )
            if not self.dry_run:
                self.nb.dcim.devices.update([device_dict])
        self.log.info("Update done")

        return updated_data_list


class ImportInterfaces(NetboxUtils):
    """ """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.required_fields = {
            "device_name",
            "site",
            "interface_name",
            "interface_type",
        }
        lint_data(
            logger=self.log,
            csv_file=self.csv_input,
            csv_data=self.data,
            required_fields=self.required_fields,
        )
        self.import_interfaces()

    @classmethod
    def __create_from__(cls, par: NetboxUtils):
        return cls(
            netbox_api=par.netbox_api,
            netbox_token=par.netbox_token,
            csv_input=par.csv_input,
            verbose=par.verbose,
            dry_run=par.dry_run,
        )

    def create_interface(self, interface_type, interface_name, device, **kwargs):
        interface = self.nb.dcim.interfaces.create(
            name=interface_name,
            device=device.id,
            type=interface_type,
            mac_address=kwargs["mac_address"],
            lag=kwargs["parent_lag"],
            cf_dhcp_enabled=kwargs["cf_dhcp_enabled"],
        )

        return interface

    def update_interface(self, interface_type, interface_name, device, **kwargs):
        interfaces = self.nb.dcim.interfaces.filter(
            name=interface_name,
        )

        for inter in interfaces:
            if inter.device.id == device.id:
                interface_id = inter.id
                break

        else:
            self.log.critical(
                f"Several interfaces match name {interface_name} of type {interface_type} for device ID'd {device.id}"
            )

        if interface_id:
            interface = self.nb.dcim.interfaces.update(
                [
                    {
                        "id": interface_id,
                        "name": interface_name,
                        "device": device.id,
                        "type": interface_type,
                        "mac_address": kwargs["mac_address"],
                        # Add remaining custom fields in csv later
                        "custom_fields": {
                            "dhcp_enabled": kwargs["cf_dhcp_enabled"],
                            "dhcp_options": None,
                            "pxe_boot_filename": None,
                            "pxe_next_server": None,
                        },
                    }
                ]
            )
            return interface

    def import_interfaces(self):
        """

        :return:
        """

        self.log.info(f"Importing new Interface data...")

        all_interfaces = []

        for item in self.data:
            address = item["address"]
            device_name = item["device_name"]
            site = item["site"].lower()
            interface_name = item["interface_name"]
            interface_type = item["interface_type"]
            if item["mac_address"]:
                mac_address = item["mac_address"]
            else:
                mac_address = None
            if item["parent_lag"]:
                parent_lag = str(item["parent_lag"])
            else:
                parent_lag = None
            if item["cf_dhcp_enabled"]:
                if item["cf_dhcp_enabled"] in [
                    "Yes",
                    "yes",
                    "YES",
                    "y",
                    "True",
                    "true",
                    "TRUE",
                ]:
                    cf_dhcp_enabled = True
                else:
                    cf_dhcp_enabled = False
            else:
                cf_dhcp_enabled = False

            device = self.nb.dcim.devices.get(
                name=device_name,
                site=site,
            )

            if device:
                self.log.debug(f"Device {device_name} exists for site {site}")
            else:
                pr = yn_prompt(
                    prompt_text=f"Device {item['device_name']} for site {item['site']} does not exist. "
                    f"Do you want to create it? (y/n) ",
                    always_yes=self.yes,
                )
                if pr and not self.dry_run:
                    try:
                        device = self.nb.dcim.devices.create(
                            name=device_name,
                            device_role=item["device_role"],
                            device_type=item["device_type"],
                            site=site,
                        )
                    except Exception as exc:
                        self.log.fatal(
                            "Input CSV file seems to be missing fields or values required to create device"
                        )
                        sys.exit(exc)

            interface = self.nb.dcim.interfaces.get(
                name=interface_name,
                device=device_name,
            )

            # Find the parent LAG interface associated to the current interface, if any
            # Does not work (nei ther interface ID or name)
            if parent_lag:
                parent_lag_select = self.nb.dcim.interfaces.get(
                    type="lag", name=parent_lag, device=device_name
                )
                if parent_lag_select:
                    parent_lag_id = parent_lag_select.id
            else:
                parent_lag_id = None

            if interface:
                if not self.dry_run:
                    interface.type = interface_type
                    interface.device_name = device_name
                    self.nb.dcim.interfaces.update([interface])
                    pr = yn_prompt(
                        prompt_text=f"Interface {interface_name} for device {device_name} "
                        f"for site {site} already exists. "
                        f"Do you want to update it? (y/n) ",
                        always_yes=self.yes,
                    )
                    if pr and not self.dry_run:
                        self.log.info(
                            f"Updating interface {interface_name} ({interface_type}) for device {device_name}"
                        )
                        self.update_interface(
                            interface_name=interface_name,
                            interface_type=interface_type,
                            device=device,
                            mac_address=mac_address,
                            parent_lag=parent_lag_id,
                            cf_dhcp_enabled=cf_dhcp_enabled,
                        )
                        interface = self.nb.dcim.interfaces.get(
                            name=interface_name,
                            device=device_name,
                        )
            else:
                pr = yn_prompt(
                    prompt_text=f"Interface {interface_name} for device {device_name} "
                    f"for site {site} does not exist. "
                    f"Do you want to create it? (y/n) ",
                    always_yes=self.yes,
                )

                if pr and not self.dry_run:
                    self.log.info(
                        f"Creating interface {interface_name} ({interface_type}) for device {device_name}"
                    )
                    interface = self.create_interface(
                        interface_name=interface_name,
                        interface_type=interface_type,
                        device=device,
                        mac_address=mac_address,
                        parent_lag=parent_lag_id,
                        cf_dhcp_enabled=cf_dhcp_enabled,
                    )

            if address:
                ip_address = self.nb.ipam.ip_addresses.get(
                    address=address, interface=interface_name
                )

                if ip_address:
                    pr = yn_prompt(
                        prompt_text=f"Existing IP address {address} "
                        f"already attributed to interface {interface_name}. "
                        f"Are you sure you want to overwrite the current "
                        f"interface attribution? (y/n) ",
                        always_yes=self.yes,
                    )

                else:
                    if not self.dry_run:
                        ip_address = self.nb.ipam.ip_addresses.create(
                            address=address,
                        )

                if pr and not self.dry_run:
                    try:
                        ip_address.assigned_object = interface
                        ip_address.assigned_object_id = interface.id
                        ip_address.assigned_object_type = "dcim.interface"
                        ip_address.save()
                        ip_address.assigned_object = device
                        ip_address.assigned_object_id = device.id
                        ip_address.assigned_object_type = "dcim.device"
                        ip_address.save()
                    # pynetbox attempts to raise an "assigned_object_type" error
                    # for adding a "dcim.device" object, it works anyway though
                    except pynetbox.core.query.RequestError:
                        continue

        return


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--netbox-token",
        type=str,
        required=False,
        default=os.environ.get("NETBOX_TOKEN", ""),
        help="Netbox API token, defaults to the environment variable NETBOX_TOKEN",
    )
    parser.add_argument(
        "--netbox-url",
        type=str,
        required=False,
        default=os.environ.get("NETBOX_API", ""),
        help="Netbox instance URL, defaults to the environment variable NETBOX_API",
    )
    parser.add_argument(
        "--csv",
        type=str,
        required=True,
        help="CSV data of the Netbox objects to update",
    )
    parser.add_argument(
        "-d",
        "--dry-run",
        required=False,
        action="store_true",
        default=False,
        help="Enable dry run (No Netbox object will be created/deleted/modified)",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        required=False,
        action="store_true",
        default=False,
        help='Enable "debug" level logs',
    )
    parser.add_argument(
        "--yes",
        required=False,
        action="store_true",
        default=False,
        help='Always answer "yes" to console prompts',
    )

    # Methods (action to take on objects in netbox)
    method_group = parser.add_mutually_exclusive_group()

    # Update devices
    method_group.add_argument(
        "--update-devices",
        action="store_const",
        dest="method",
        const="update_devices",
        help="Update devices from CSV",
    )

    # Import interfaces
    method_group.add_argument(
        "--import-interfaces",
        action="store_const",
        dest="method",
        const="import_interfaces",
        help="Import interfaces from CSV",
    )

    arguments = parser.parse_args()

    _netbox_token = arguments.netbox_token
    _netbox_api = arguments.netbox_url
    _csv_input = arguments.csv
    _method = arguments.method
    print(arguments.dry_run)
    if _method == "update_devices":
        NU = UpdateDevices(
            netbox_token=_netbox_token,
            netbox_api=_netbox_api,
            csv_input=_csv_input,
            verbose=arguments.verbose,
            dry_run=arguments.dry_run,
            yes=arguments.yes,
        )

    elif _method == "import_interfaces":
        NU = ImportInterfaces(
            netbox_token=_netbox_token,
            netbox_api=_netbox_api,
            csv_input=_csv_input,
            verbose=arguments.verbose,
            dry_run=arguments.dry_run,
            yes=arguments.yes,
        )
