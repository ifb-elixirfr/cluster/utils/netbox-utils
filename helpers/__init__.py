#!/usr/bin/env python3
# -*- coding:utf-8 -*-

"""
Usage

Interfaces (dry run, verbose):
python3 netbox_utils.py --import-interfaces --csv interfaces.csv --dry-run --verbose --yes

"""

import sys
import os
import csv

import logging as log


def yn_prompt(prompt_text, always_yes=False):
    """


    :param prompt_text:
    :param always_yes:
    :return:
    """

    if always_yes:
        return True

    __prompt = input(prompt_text)
    if __prompt in {"yes", "Yes", "y", "Y"}:
        return True

    return False


def lint_data(logger: log.Logger, csv_data: list, csv_file: str, required_fields: set):
    """
    Lint CSV data (dict) to ensure all columns are present and there are no empty values

    :param logger:
    :param csv_data:
    :param csv_file:
    :param required_fields:
    :return:
    """

    required_fields = set(required_fields)
    encountered_fields = set()
    for row in csv_data:
        for key, value in row.items():
            if value == "" or not value:
                logger.warning(
                    f'Empty value for column "{key}" in input CSV data ({csv_file})'
                )
            encountered_fields.add(key)

        for field in required_fields:
            if field not in encountered_fields:
                logger.fatal(
                    f'Required column "{field}" not present in input CSV data ({csv_file}'
                )
                sys.exit(1)

    return True


def init_logger(verbose: bool = False) -> log.Logger:
    """
    Create basic logger with 2 levels (debuginfo/debug)

    :param verbose:
    :return:
    """

    logger = log.getLogger("netbox-utils")
    console_handler = log.StreamHandler()
    if verbose:
        logger.setLevel(log.DEBUG)
        console_handler.setLevel(log.DEBUG)
    else:
        logger.setLevel(log.INFO)
        console_handler.setLevel(log.INFO)
    formatter = log.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)

    return logger


def parse_input_data(csv_file: str):
    """
    Parse the input CSV data into a list of dicts (one row = one dict)

    :param csv_file:
    :return:
    """

    csv_data = []
    with open(csv_file, "r", encoding="utf-8") as csv_handle:
        csv_reader = csv.DictReader(csv_handle)
        for row in csv_reader:
            csv_data.append(row)

    return csv_data