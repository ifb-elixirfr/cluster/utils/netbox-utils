import pynetbox
import sys
import os
import argparse

import logging
logger = logging.getLogger(os.path.basename(__file__))


def main(netbox_token, netbox_url, tag, target_env, method, name_filter, type_filter, dry_run=False, ):
    """ """

    ###
    logging.basicConfig(level=logging.INFO)
    nb = pynetbox.api(url=netbox_url, token=netbox_token)

    vms = nb.virtualization.virtual_machines.all()
    devices = nb.dcim.devices.all()

    for vm in [x for x in vms if x.status["value"] == "active"]:
            if (
                target_env in [x["display"] for x in vm["tags"]]
                and name_filter in vm["name"]
            ):
                if type_filter:
                    if type_filter in ["vm", ""]:
                        logger.info(f"virtual machine {vm} matches filters")
                        if method == "add":
                            logger.info(f"adding tag {tag} on VM {vm}")
                            if not dry_run:
                                vm.tags.append({"slug": tag})
                                nb.virtualization.virtual_machines.update([vm])
                        elif method == "delete":
                            index_to_remove = None
                            for i, d in enumerate(vm.tags):
                                if d["slug"] == tag:
                                    index_to_remove = i
                                    break
                            if index_to_remove:
                                logger.info(f"deleting tag {tag} on VM {vm}")
                                if not dry_run:
                                    vm.tags.pop(index_to_remove)
                                    nb.virtualization.virtual_machines.update([vm])
                    else:
                        logger.info("VM skipped because of supplied type_filter")
                else:
                    logger.info(f"virtual machine {vm} matches filters")
                    if method == "add":
                        logger.info(f"adding tag {tag} on VM {vm}")
                        if not dry_run:
                            vm.tags.append({"slug": tag})
                            nb.virtualization.virtual_machines.update([vm])
                    elif method == "delete":
                        index_to_remove = None
                        for i, d in enumerate(vm.tags):
                            if d["slug"] == tag:
                                index_to_remove = i
                                break
                        if index_to_remove:
                            logger.info(f"deleting tag {tag} on VM {vm}")
                            if not dry_run:
                                vm.tags.pop(index_to_remove)
                                nb.virtualization.virtual_machines.update([vm])

    for device in [x for x in devices if x.status["value"] == "active"]:
        if (
            target_env in [x["display"] for x in device["tags"]]
            and name_filter in device["name"]
        ):
            if type_filter:
                if type_filter in ["device", ""]:
                    logger.info(f"device {device} matches filters")
                    if method == "add":
                        logger.info(f"adding tag {tag} on device {device}")
                        if not dry_run:
                            device.tags.append({"slug": tag})
                            nb.dcim.devices.update([device])
                    elif method == "delete":
                        index_to_remove = None
                        for i, d in enumerate(device.tags):
                            if d["slug"] == tag:
                                index_to_remove = i
                                break
                        if index_to_remove:
                            logger.info(f"deleting tag {tag} on device {device}")
                            if not dry_run:
                                device.tags.pop(index_to_remove)
                                nb.dcim.devices.update([device])
                else:
                    logger.info("Device skipped because of supplied type_filter")
            else:
                logger.info(f"device {device} matches filters")
                if method == "add":
                    logger.info(f"adding tag {tag} on device {device}")
                    if not dry_run:
                        device.tags.append({"slug": tag})
                        nb.dcim.devices.update([device])
                elif method == "delete":
                    index_to_remove = None
                    for i, d in enumerate(device.tags):
                        if d["slug"] == tag:
                            index_to_remove = i
                            break
                    if index_to_remove:
                        logger.info(f"deleting tag {tag} on device {device}")
                        if not dry_run:
                            device.tags.pop(index_to_remove)
                            nb.dcim.devices.update([device])


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--netbox-token",
        default=os.environ.get("NETBOX_TOKEN"),
        required=False,
        type=str,
    )
    parser.add_argument(
        "--netbox-url",
        default="https://inventory.cluster.france-bioinformatique.fr",
        required=False,
        type=str,
    )
    parser.add_argument(
        "--tag", help="Tag to add/remove to matching machines", required=True, type=str
    )
    parser.add_argument(
        "--name-filter",
        help="Filter on name, "
        "non-exact (will match all machines who contain the "
        "provided string in their name)",
        required=False,
        type=str,
    )
    parser.add_argument(
        "--type-filter",
        help="Filter on machine type (vm, device)",
        required=False,
        type=str
    )
    parser.add_argument(
        "--target-env", help="Filter on tag name", required=True, type=str
    )
    parser.add_argument("--dry-run", help="Do not add/delete tags, only log would-be changes",
                        required=False, action="store_true", default=False)

    method_group = parser.add_mutually_exclusive_group(required=True)
    method_group.add_argument("--add", action="store_const", const="add", dest="method")
    method_group.add_argument(
        "--delete", action="store_const", const="delete", dest="method"
    )

    args = parser.parse_args()

    try:
        _netbox_token = args.netbox_token
        _netbox_url = args.netbox_url
        _tag = args.tag
        _target_env = args.target_env
        _method = args.method
        _dry_run = args.dry_run
        if args.name_filter:
            _name_filter = args.name_filter
        else:
            _name_filter = ""
        if args.type_filter:
            if args.type_filter not in ["vm", "device"]:
                logger.critical(f"Supplied value for argument --type-filter must match one of the following "
                                f"values: ['vm', 'device']")
                sys.exit()
            else:
                _type_filter = args.type_filter
        else:
            _type_filter = ""
        if not _netbox_token:
            logger.critical(
                f"could not find a valid Netbox API token for URL {_netbox_url} in command line arguments or "
                f"in the environment"
            )
            sys.exit()
    except Exception as exc:
        logger.critical(exc)
        sys.exit(1)

    main(
        netbox_token=_netbox_token,
        netbox_url=_netbox_url,
        tag=_tag,
        target_env=_target_env,
        method=_method,
        name_filter=_name_filter,
        type_filter=_type_filter,
        dry_run=_dry_run,
    )
